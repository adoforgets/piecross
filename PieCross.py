import tkinter, subprocess, os, win32api, win32con, pywintypes, sys

main = tkinter.Tk()

main.title("PieCross")
main.iconbitmap("img/piecross-logo.ico")
main.minsize(256 , 128)

submain = tkinter.Frame(main, bg = "black")
submain.pack(fill =  tkinter.BOTH, expand = 1)
submain.highlightbackground = "black"


def crsinit():
	global proc
	#proc = subprocess.Popen(["gcc","overlay.c"])
	proc = subprocess.Popen(["crosshair/crosshair.exe"])
	#proc = os.system("dist/gui.exe")

def crsterm():
	#subprocess.Popen.kill("dist/gui.exe")
	os.system("taskkill /f /im crosshair.exe")




initbutton = tkinter.Button( submain, text = "Start Crosshair", command = crsinit, bg = "black", fg = "white")
termbutton = tkinter.Button( submain, text = "Stop Crosshair", command = crsterm, bg = "black", fg = "white")

initbutton.pack()
termbutton.pack()

def appclose():
	try: 
		crsterm()
	finally:
		main.destroy()
	

main.protocol("WM_DELETE_WINDOW", appclose)
main.mainloop()