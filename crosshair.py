# This program is written in python 3
# It's for overlaying a cross hair on the screen. It is for windows only.
# Source code from https://stackoverflow.com/questions/21840133/how-to-display-text-on-the-screen-without-a-window-using-python
# and from https://stackoverflow.com/questions/3352918/how-to-center-a-window-on-the-screen-in-tkinter

#class crosshair:
#	"The overlay code itself"
#	def __init__(self):
import tkinter, win32api, win32con, pywintypes



root = tkinter.Tk()
root.title("root")

crosshair = tkinter.PhotoImage(file="img/crosshairs/CrossHair0.gif")
label = tkinter.Label(image= crosshair, fg='white', bg='black')
label.master.overrideredirect(True)
label.master.geometry("+250+250")
label.master.lift()
label.master.wm_attributes("-topmost", True)
label.master.wm_attributes("-disabled", True)
label.master.wm_attributes("-transparentcolor", "black")

hWindow = pywintypes.HANDLE(int(label.master.frame(), 16))
# http://msdn.microsoft.com/en-us/library/windows/desktop/ff700543(v=vs.85).aspx
# The WS_EX_TRANSPARENT flag makes events (like mouse clicks) fall through the window.
exStyle = win32con.WS_EX_COMPOSITED | win32con.WS_EX_LAYERED | win32con.WS_EX_NOACTIVATE | win32con.WS_EX_TOPMOST | win32con.WS_EX_TRANSPARENT
win32api.SetWindowLong(hWindow, win32con.GWL_EXSTYLE, exStyle)

# Apparently a common hack to get the window size. Temporarily hide the
# window to avoid update_idletasks() drawing the window in the wrong
# position.
root.withdraw()
root.update_idletasks()  # Update "requested size" from geometry manager

x = (root.winfo_screenwidth() - root.winfo_reqwidth()) / 2
y = (root.winfo_screenheight() - root.winfo_reqheight()) / 2
root.geometry("+%d+%d" % (x, y))

# This seems to draw the window frame immediately, so only call deiconify()
# after setting correct window position
label.place(relheight = 1.0, relwidth = 1.0)
root.deiconify()

label.mainloop()
root.mainloop()

#crosshair()