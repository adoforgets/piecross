# PieCross

Just what ***is*** PieCross?

It's a screen overlay! Some friends of mine were trying to get a cross hair in a game called Dead By Daylight.<br />
I didn't trust the programs they were using to do it, so I wrote one for them.<br />
It's written in Python 3 and utilizes Windows libraries to get the job done<br />
That said, this only works on Windows<br />

Special Note! I don't really understand Git, so the source code available here is the basic stuff, <br />
as in just the python files that were compiled into an executable. I will get eh full source uploaded as soon as I can!

To change your cross hair, you'll have to edit the image: PieCross Bundle/PieCross/img/crosshairs/crosshair0.gif<br />
You must export it as a gif<br />
Try to keep it under 128 x 128 (This is untested, I only know for sure that 64 x 64 or less works)<br />
Black is the color that's being keyed out, keep that in mind<br />

To run PieCross, just open the "PieCross" folder and run "PieCross.exe".<br />
It should just run out of the box, but keep in mind that the application<br />
you're trying to display over must be in Full Screen Windowed (aka Borderless Window).<br />
Most games support this, or they default to it (Dead by Daylight for example).<br />

Planned Features in order of priority:<br />
<ul>
<li>Change the cross hair you're using without having to restart (have multiple cross hairs available)</li>
<li>Ability to have the cross hair offset from the center</li>
<li>Full screen over lays</li>
<li>Animated overlays?</li>
<li>Cross Platform support</li>
</ul><br />
Sources: (I blatantly copied some code from stack overflow posts)<br />
https://stackoverflow.com/questions/21840133/how-to-display-text-on-the-screen-without-a-window-using-python<br />
https://stackoverflow.com/questions/3352918/how-to-center-a-window-on-the-screen-in-tkinter<br />

PieCross is licensed under GNU General Public License v3.0<br />
Which you can find here:<br />
https://choosealicense.com/licenses/gpl-3.0/<br />

PS:<br />
This project is very much in Alpha.<br />
It might be horribly buggy,<br />
it might not run at all,<br />
such is the nature of development<br />

~Adoforgets<br />